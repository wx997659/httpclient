/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Credentials, HttpClient, NetAuthenticator, Response,Request } from '@ohos/httpclient'
import http from '@ohos.net.http';
import prompt from '@system.prompt';

@Entry
@Component
struct authenticator_sample {
  @State state: string = ''
  @State content: string = ''
  count: number = 0

  authenticator() {
    prompt.showToast({ message: 'http:authenticator' })
    this.clientServer()
  }

  clientServer() {
    this.state = '请求地址：\nhttps://publicobject.com/secrets/hellosecret.txt'
    this.state += '\n请求状态：正在请求中'
    this.content = ''
    let that = this
    let credentials: string = Credentials.basic("jesse", "password1");
    let httpRequest = http.createHttp();
    // 用于订阅http响应头，此接口会比request请求先返回。可以根据业务需要订阅此消息
    // 从API 8开始，使用on('headersReceive', Callback)替代on('headerReceive', AsyncCallback)。 8+
    httpRequest.on('headersReceive', (header) => {
      console.info('header1: ' + JSON.stringify(header));
    });
    httpRequest.request(
      // 填写http请求的url地址，可以带参数也可以不带参数。URL地址需要开发者自定义。请求的参数可以在extraData中指定
      "https://publicobject.com/secrets/hellosecret.txt",
      {
        method: http.RequestMethod.GET, // 可选，默认为http.RequestMethod.GET
        // 开发者根据自身业务需要添加header字段
        header: {
          //'Authorization':credentials,
          "content_type": "application/json"
        },
        connectTimeout: 60000, // 可选，默认为60s
        readTimeout: 60000, // 可选，默认为60s
      }, (err, data) => {
      if (!err) {
        // data.result为http响应内容，可根据业务需要进行解析
        console.info('Result2:' + data.result);
        console.info('code2:' + data.responseCode);
        if (data.responseCode == 401) {
          that.state += '\n请求状态： 401,需要身份认证'
          that.state += '\n请求状态： 身份认证用户名：jesse，密码：password1'
          that.state += '\n认证凭证： Authorization:' + credentials
          that.state += '\n请求状态： 身份认证开始，重新请求中'
          let request: Request = new Request.Builder()
            .get('https://publicobject.com/secrets/hellosecret.txt')
            .addHeader("Content-Type", "application/json")
            .build()

          const myResponse: Response = new Response()
          myResponse.responseCode = data.responseCode
          myResponse.result = data.result.toString()
          myResponse.header = JSON.stringify(data.header)

          let requestResult: Request = new NetAuthenticator('jesse', 'password1').authenticate(request, myResponse);
          console.info('method:' + request.method);
          console.info('method:headers:' + JSON.stringify(request.headers));
          setTimeout(() => {
            that.auth(requestResult);
          }, 1500)
        }
        console.info('header2:' + JSON.stringify(data.header));
      } else {
        console.info('error:' + JSON.stringify(err));
        that.content = JSON.stringify(err)
        httpRequest.destroy();
      }
    }
    );
  }

  auth(requestRes: Request) {
    console.info('requestRes:method:' + JSON.stringify(requestRes.method))
    console.info('requestRes:headers:' + JSON.stringify(requestRes.headers))
    let that = this
    let credentials: string = Credentials.basic("jesse", "password1");
    let httpRequest = http.createHttp();
    // 用于订阅http响应头，此接口会比request请求先返回。可以根据业务需要订阅此消息
    // 从API 8开始，使用on('headersReceive', Callback)替代on('headerReceive', AsyncCallback)。 8+
    httpRequest.on('headersReceive', (header) => {
      console.info('header1: ' + JSON.stringify(header));
    });
    httpRequest.request(
      // 填写http请求的url地址，可以带参数也可以不带参数。URL地址需要开发者自定义。请求的参数可以在extraData中指定
      "https://publicobject.com/secrets/hellosecret.txt",
      {
        method: requestRes.method, // 可选，默认为http.RequestMethod.GET
        // 开发者根据自身业务需要添加header字段
        header: requestRes.headers,
        connectTimeout: 60000, // 可选，默认为60s
        readTimeout: 60000, // 可选，默认为60s
      }, (err, data) => {
      if (!err) {
        // data.result为http响应内容，可根据业务需要进行解析
        console.info('Result2:' + data.result);
        that.state += '\n请求状态： 身份认证结束，请求成功，内容如下：'
        that.content = data.result + ''
        // data.header为http响应头，可根据业务需要进行解析
        console.info('header2:' + JSON.stringify(data.header));
        console.info('cookies2:' + data.cookies); // 8+
      } else {
        console.info('error:' + JSON.stringify(err));
        that.content = JSON.stringify(err)
        // 当该请求使用完毕时，调用destroy方法主动销毁。
        httpRequest.destroy();
      }
    }
    );
  }

  build() {
    Column() {
      Flex({
        direction: FlexDirection.Column
      }) {
        Navigator({
          target: "",
          type: NavigationType.Back
        }) {
          Text('BACK')
            .fontSize(12)
            .border({
              width: 1
            })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')
      .padding(10)

      Flex({
        direction: FlexDirection.Column
      }) {
        Button('身份认证')
          .width('80%')
          .height('100%')
          .fontSize(18)
          .fontColor(0xCCCCCC)
          .align(Alignment.Center)
          .margin(10)
          .onClick((event: ClickEvent) => {
            this.authenticator()
          })
      }
      .height('10%')
      .width('100%')
      .padding(10)

      Text(this.state).width('100%').fontSize(18).fontColor(Color.Black)
      Text(this.content)
        .width('100%')
        .layoutWeight(1)
        .fontSize(10)
        .fontColor(Color.Black)
        .textAlign(TextAlign.Center)

    }.width('100%').margin({
      top: 5,
      bottom: 100
    }).height('100%')
  }
}