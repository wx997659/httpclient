/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  Dns,
  HttpClient,
  Request,
  Response,
  TimeUnit,
  CertificatePinnerBuilder,
  Logger
} from '@ohos/httpclient';
import connection from '@ohos.net.connection';
import { Utils } from "../utils/Utils";
import { BusinessError } from '@ohos.base';

import { CertificatePinner } from '@ohos/httpclient/src/main/ets/CertificatePinner';

@Entry
@Component
struct certificatePinner {
  @State url: string | undefined = 'https://1.94.37.200:8080/user/getUserByUuid?userUuid=1';
  @State result: string | undefined = '响应结果';
  @State hostname: string | undefined = '1.94.37.200';
  @State successPin: string | undefined = 'sha1/f58c753eff28a6c9847775c4bc90f023b44dfd41';
  @State failPin: string | undefined = 'sha1/f58c753eff28a6c';
  ca: string | undefined = 'caPin.crt';

  build() {
    Column() {
      Row() {
        Navigator({
          target: '',
          type: NavigationType.Back
        }) {
          Text('BACK')
            .fontSize(10)
            .border({
              width: 1
            })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')

      TextInput({ text: this.url, placeholder: '请输入URL' })
        .placeholderColor('#ffffff')
        .caretColor(Color.Blue)
        .height('150px')
        .fontSize('18fp')
        .onChange((value: string) => {
          this.url = value
        })
        .margin(10)

      TextInput({ text: this.hostname, placeholder: '请输入hostname' })
        .placeholderColor('#ffffff')
        .caretColor(Color.Blue)
        .height('150px')
        .fontSize('18fp')
        .onChange((value: string) => {
          this.hostname = value
        })
        .margin(10)

      TextInput({ text: this.successPin, placeholder: '请输入成功指纹' })
        .placeholderColor('#ffffff')
        .caretColor(Color.Blue)
        .height('150px')
        .fontSize('18fp')
        .onChange((value: string) => {
          this.successPin = value
        })
        .margin(10)

      TextInput({ text: this.failPin, placeholder: '请输入失败指纹' })
        .placeholderColor('#ffffff')
        .caretColor(Color.Blue)
        .height('150px')
        .fontSize('18fp')
        .onChange((value: string) => {
          this.failPin = value
        })
        .margin(10)

      Button('发起请求-校验通过')
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async () => {
          let certificatePinner: CertificatePinner | undefined = new CertificatePinnerBuilder()
            .add('1.94.37.200', this.successPin)
            .build()
          let client: HttpClient | undefined = new HttpClient
            .Builder()
            .dns(new CustomDns())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          let context: Context | undefined = getContext();
          let CA: string | undefined = await new Utils().getCA(this.ca as string, context);
          let request: Request | undefined = new Request.Builder()
            .url(this.url)
            .method('GET')
            .ca([CA])
            .build();
          client.newCall(request)
            .setCertificatePinner(certificatePinner)
            .enqueue((result: Response) => {
              this.result = '响应结果success' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4)
              Logger.info('证书锁定---success---' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4));
              certificatePinner = undefined
              client = undefined
              context = undefined
              CA = undefined
              request = undefined
            }, (err: BusinessError) => {
              this.result = '响应结果fail' + JSON.stringify(err)
              Logger.info('证书锁定---failed--- ', JSON.stringify(err));
              certificatePinner = undefined
              client = undefined
              context = undefined
              CA = undefined
              request = undefined
            });
        })

      Button('发起请求-校验失败')
        .width('80%')
        .height('80px')
        .fontSize(18)
        .fontColor(0xCCCCCC)
        .align(Alignment.Center)
        .margin(10)
        .onClick(async () => {
          let certificatePinner: CertificatePinner | undefined = new CertificatePinnerBuilder()
            .add('1.94.37.200', this.failPin)
            .build();
          let client: HttpClient | undefined = new HttpClient
            .Builder()
            .dns(new CustomDns())
            .setConnectTimeout(10, TimeUnit.SECONDS)
            .setReadTimeout(10, TimeUnit.SECONDS)
            .build();
          let context: Context | undefined = getContext();
          let CA: string | undefined = await new Utils().getCA(this.ca as string, context);
          let request: Request | undefined = new Request.Builder()
            .url(this.url)
            .method('GET')
            .ca([CA])
            .build();
          client.newCall(request)
            .setCertificatePinner(certificatePinner)
            .enqueue((result: Response) => {
              this.result = '响应结果success' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4);
              Logger.info('证书锁定---success---' + JSON.stringify(JSON.parse(JSON.stringify(result)), null, 4));
              certificatePinner = undefined
              client = undefined
              context = undefined
              CA = undefined
              request = undefined
            }, (err: BusinessError) => {
              this.result = '响应结果fail' + JSON.stringify(err);
              Logger.info('证书锁定---failed--- ', JSON.stringify(err));
              certificatePinner = undefined
              client = undefined
              context = undefined
              CA = undefined
              request = undefined
            });
        })

      Scroll() {
        Column() {
          Text(this.result)
            .width('80%')
            .fontSize("18fp")
            .margin(10);
        };
      }
      .width('100%')
      .layoutWeight(1);
    }
    .width('100%')
    .height('100%');
  };

  aboutToDisappear(): void {
    this.url = undefined
    this.result = undefined
    this.hostname = undefined
    this.successPin = undefined
    this.failPin = undefined
    this.ca = undefined
  }

  Uint8ArrayToString(fileData: Uint8Array): string {
    let dataString = '';
    for (let i = 0; i < fileData.length; i++) {
      dataString += String.fromCharCode(fileData[i]);
    }
    return dataString;
  };
}


export class CustomDns implements Dns {
  async lookup(hostname: string): Promise<Array<connection.NetAddress>> {
    console.info('DNSTEST CustomDns begin here');
    return await new Promise((resolve, reject) => {
      let netAddress: Array<connection.NetAddress> = [{ 'address': '1.94.37.200', 'family': 1, 'port': 8080 }];
      ;
      resolve(netAddress);
    })
  };
}