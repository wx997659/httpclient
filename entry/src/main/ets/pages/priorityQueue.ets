/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { HttpClient, Logger, Request, Response, TimeUnit } from '@ohos/httpclient'
import { BusinessError } from '@ohos.base';

@Entry
@Component
struct priorityQueue {
  @State queueCount: string = '';
  @State maxCallCount: string = '最大请求队列数为： 64';
  @State maxHostCallCount: string = '最大同Host请求数为： 5';
  maxQueueCount: number = 64;
  maxHostCount: number = 5;
  client: HttpClient = new HttpClient.Builder()
    .setConnectTimeout(5, TimeUnit.SECONDS)
    .setReadTimeout(5, TimeUnit.SECONDS)
    .setWriteTimeout(5, TimeUnit.SECONDS)
    .build();

  createAndRequestCall() {
    for (let i = 1; i < 201; i++) {
      Logger.info('Dispatcher 创建第 ' + i + ' 例优先级为0的请求');
      this.queueCount = '详情：运行中队列 ' + this.client.dispatcher.getRunningCallsCount() + ' 例，等待队列 ' +
        this.client.dispatcher.getReadyCallsCount() + ' 例';
      let request: Request = new Request.Builder()
        .url('http://hshapp.ncn.com.cn/wisdom3/config/config.do')
        .get()
        .setPriority(0)
        .build();
      this.client.newCall(request).enqueue((result: Response) => {
        Logger.info('Dispatcher 第 ' + i + ' 例优先级为0的请求完成');
        this.queueCount = '详情：运行中队列 ' + this.client.dispatcher.getRunningCallsCount() + ' 例，等待队列 ' +
          this.client.dispatcher.getReadyCallsCount() + ' 例';
      }, (err: BusinessError) => {
        Logger.info('Dispatcher 第 ' + i + ' 例优先级为0的请求完成');
        this.queueCount = '详情：运行中队列 ' + this.client.dispatcher.getRunningCallsCount() + ' 例，等待队列 ' +
          this.client.dispatcher.getReadyCallsCount() + ' 例';
      });
    }

    this.createHighPriorityCall(1);
    this.createHighPriorityCall(2);
    this.createHighPriorityCall(3);
    this.createHighPriorityCall(4);
    this.createHighPriorityCall(5);
    this.createHighPriorityCall(6);
    this.createHighPriorityCall(7);
    this.createHighPriorityCall(8);
    this.createHighPriorityCall(9);
    this.createHighPriorityCall(10);
  }

  createHighPriorityCall(priority: Number) {
    Logger.info('Dispatcher 创建优先级为' + priority + ' 的请求');
    this.queueCount = '详情：运行中队列 ' + this.client.dispatcher.getRunningCallsCount() + ' 例，等待队列 ' +
      this.client.dispatcher.getReadyCallsCount() + ' 例';
    let request: Request = new Request.Builder()
      .url('http://hshapp.ncn.com.cn/wisdom3/config/config.do')
      .get()
      .setPriority(priority)
      .build();
    this.client.newCall(request).enqueue((result: Response) => {
      Logger.info('Dispatcher 优先级为 ' + priority + '的请求完成');
      this.queueCount = '详情：运行中队列 ' + this.client.dispatcher.getRunningCallsCount() + ' 例，等待队列 ' +
        this.client.dispatcher.getReadyCallsCount() + ' 例';
    }, (err: BusinessError) => {
      Logger.info('Dispatcher 优先级为 ' + priority + '的请求完成');
      this.queueCount = '详情：运行中队列 ' + this.client.dispatcher.getRunningCallsCount() + ' 例，等待队列 ' +
        this.client.dispatcher.getReadyCallsCount() + ' 例';
    });
  }

  build() {
    Column() {

      Flex({
        direction: FlexDirection.Column
      }) {
        Navigator({
          target: '',
          type: NavigationType.Back
        }) {
          Text('BACK')
            .fontSize(12)
            .border({
              width: 1
            })
            .padding(10)
            .fontColor(0x000000)
            .borderColor(0x317aff)
        }
      }
      .height('10%')
      .width('100%')
      .padding(10)

      TextInput({ placeholder: '请输入新的最大请求队列数' })
        .placeholderColor(Color.Grey)
        .placeholderFont({ size: 14, weight: 400 })
        .caretColor(Color.Blue)
        .width(400)
        .height(40)
        .margin(20)
        .fontSize(14)
        .fontColor(Color.Black)
        .type(InputType.Number)
        .onChange((value: string) => {
          this.maxQueueCount = Number.parseInt(value);
        })

      Button('确定修改')
        .width('50%')
        .height(40)
        .fontSize(18)
        .fontColor(0xFFFFFF)
        .align(Alignment.Center)
        .margin(10)
        .onClick(() => {
          if (this.maxQueueCount <= 0) {
            Logger.info('Dispatcher 设置数值不对');
          } else {
            this.maxCallCount = '最大请求队列数为： ' + this.maxQueueCount;
            this.client.dispatcher.setMaxRequestCount(this.maxQueueCount);
          }
        })

      Text(this.maxCallCount)
        .fontColor(0X0000FF)
        .width('80%')
        .fontSize('14fp')
        .margin(20)

      TextInput({ placeholder: '请输入新的最大同Host请求数' })
        .placeholderColor(Color.Grey)
        .placeholderFont({ size: 14, weight: 400 })
        .caretColor(Color.Blue)
        .width(400)
        .height(40)
        .margin(20)
        .fontSize(14)
        .fontColor(Color.Black)
        .type(InputType.Number)
        .onChange((value: string) => {
          this.maxHostCount = Number.parseInt(value);
        })

      Button('确定修改')
        .width('50%')
        .height(40)
        .fontSize(18)
        .fontColor(0xFFFFFF)
        .align(Alignment.Center)
        .margin(10)
        .onClick(() => {
          if (this.maxHostCount <= 0) {
            Logger.info('Dispatcher 设置数值不对');
          } else {
            this.maxHostCallCount = '最大同Host请求数为： ' + this.maxHostCount;
            this.client.dispatcher.setMaxRequestPreHostCount(this.maxHostCount);
          }
        })

      Text(this.maxHostCallCount)
        .fontColor(0X0000FF)
        .width('80%')
        .fontSize('14fp')
        .margin(20)

      Button('发起请求')
        .width('50%')
        .height(40)
        .fontSize(18)
        .fontColor(0xFFFFFF)
        .align(Alignment.Center)
        .margin(10)
        .onClick(() => {
          this.createAndRequestCall()
        })

      Text(this.queueCount)
        .fontColor(0X0000FF)
        .width('80%')
        .fontSize('14fp')
        .margin(20)

    }
    .justifyContent(FlexAlign.Start)
    .width('100%')
    .height('100%')
  }
}